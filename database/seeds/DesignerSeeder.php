<?php

use Illuminate\Database\Seeder;
use App\Designer;
use Illuminate\Support\Facades\DB;

class DesignerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $designer = [
            ['name' => 'A Bathing Ape'],
            ['name' => 'A.W.A.K.E'],
            ['name' => 'Acler'],
            ['name' => 'Adam by Adam Lippes'],
            ['name' => 'Adidas by Rick Owens'],
            ['name' => 'Adrianna Papell'],
            ['name' => 'B By Brian Atwood'],
            ['name' => 'Bagutta'],
            ['name' => 'Balenciaga'],
            ['name' => 'C/MEO COLLECTIVE'],
            ['name' => 'Calvin Klein'],
            ['name' => 'Camilla And Marc'],
            ['name' => 'Capitol Couture by Trish Summerville'],
            ['name' => 'D&G'],
            ['name' => 'David Koma'],
            ['name' => 'Each x Other'],
            ['name' => 'Fabiana Filippi'],
            ['name' => 'G Star Raw'],
        ];
        DB::table('designers')->insert($designer);
        // Designer::create($designer);
    }
}
