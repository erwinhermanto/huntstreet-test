@extends('layouts.invitation')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                {{-- <div class="card-header">Dashboard</div> --}}

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <script>
                        CountDownTimer('2020-11-20 00:00:00', 'countdown');
                        function CountDownTimer(dt, id)
                        {
                            var end = new Date('2021-11-20 00:00:00');
                            var _second = 1000;
                            var _minute = _second * 60;
                            var _hour = _minute * 60;
                            var _day = _hour * 24;
                            var timer;
                            function showRemaining() {
                                var now = new Date();
                                var distance = end - now;
                                if (distance < 0) {

                                    clearInterval(timer);
                                    document.getElementById(id).innerHTML = '<b>Invitation SUDAH BERAKHIR</b> ';
                                    return;
                                }
                                var days = Math.floor(distance / _day);
                                var hours = Math.floor((distance % _day) / _hour);
                                var minutes = Math.floor((distance % _hour) / _minute);
                                var seconds = Math.floor((distance % _minute) / _second);

                                document.getElementById(id).innerHTML = days + 'days ';
                                document.getElementById(id).innerHTML += hours + 'hrs ';
                                document.getElementById(id).innerHTML += minutes + 'mins ';
                                document.getElementById(id).innerHTML += seconds + 'secs';
                                document.getElementById(id).innerHTML +='<h2>Invitation BELUM BERAKHIR</h2>';
                            }
                            timer = setInterval(showRemaining, 1000);
                        }
                    </script>
                    <div id="countdown">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
