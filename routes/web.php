<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/home/send_email', 'HomeController@send')->name('send_email');
Route::get('/invitation/{email}', 'InvitationController@invitation')->name('invitation');
Route::post('/invitation/{email}', 'InvitationController@post_invitation')->name('post_invitation');
