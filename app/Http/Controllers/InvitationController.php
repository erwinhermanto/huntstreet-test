<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invitation;
use App\Designer;

class InvitationController extends Controller
{
    public function invitation($email)
    {
        $data = Invitation::where('email',$email)->first();
        $data_designer = Designer::get();
        // dd($data_designer);
        if ($data['name'] == null) {
            return view('invitation.invitation', compact('data_designer','data'));
        } else {
            return view('invitation.countdown', compact('data_designer','data'));
        }
        return $email;
    }

    public function post_invitation(Request $request, $email)
    {
        $email = $request->route('email');
        $data_designer = Designer::get();
        $data = Invitation::where('email',$email)->first();
        // dd($request->designer);
        Invitation::where('email', $email)->update([
            'name' => $request->name,
            'born_date' => $request->born_date,
            'gender' => $request->gender,
            'designer' => $request->designer,
        ]);

        return view('invitation.countdown', compact('data_designer','data'));
    }
}
