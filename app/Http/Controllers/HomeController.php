<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invitation;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function send(Request $req)
    {
        $req->validate([
            'email' => 'required|email|unique:invitations,email',
        ]);
        Invitation::create([
            'email' => $req['email'],
        ]);
        $currentURL = \URL::to('/');
        $url = $currentURL.'/invitation/'.$req['email'];
        $details = [
            'title' => 'Mail from HUNTBAZAAR',
            'body' => 'you get invitation from HuntStreet, please click this link and fill this form '.$url
        ];

        \Mail::to($req['email'])->send(new \App\Mail\MyTestMail($details));
        return redirect('home');
    }

}
